package com.vimtura.vimphone;
/*
DialerFragment.java
Copyright (C) 2012  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCore;
import org.linphone.mediastream.Log;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.vimtura.vimphone.R;
import com.vimtura.vimphone.ui.AddressAware;
import com.vimtura.vimphone.ui.Numpad;
import com.vimtura.vimphone.ui.AddressText;
import com.vimtura.vimphone.ui.CallButton;
import com.vimtura.vimphone.ui.EraseButton;

/**
 * @author Sylvain Berfini
 */
public class DialerFragment extends Fragment {
	private static DialerFragment instance;
	private static boolean isCallTransferOngoing = false;
	
	public boolean mVisible;
	private AddressText mAddress;
	private CallButton mCall;
	private ImageView mAddContact;
	private AddressAware numpad;
	private OnClickListener addContactListener, cancelListener, transferListener;
	private boolean shouldEmptyAddressField = true;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
		instance = this;
        View view = inflater.inflate(R.layout.dialer, container, false);
		
		mAddress = (AddressText) view.findViewById(R.id.Adress); 
		mAddress.setDialerFragment(this);
		
		EraseButton erase = (EraseButton) view.findViewById(R.id.Erase);
		erase.setAddressWidget(mAddress);
		
		mCall = (CallButton) view.findViewById(R.id.Call);
		mCall.setAddressWidget(mAddress);
		if (LinphoneActivity.isInstanciated() && LinphoneManager.getLc().getCallsNb() > 0) {
			if (isCallTransferOngoing) {
				mCall.setImageResource(R.drawable.transfer_call);
			} else {
				mCall.setImageResource(R.drawable.add_call);
			}
		} else {
			mCall.setImageResource(R.drawable.call);
		}
		
		numpad = (AddressAware) view.findViewById(R.id.Dialer);
		if (numpad != null) {
			numpad.setAddressWidget(mAddress);
		}
		
		mAddContact = (ImageView) view.findViewById(R.id.addContact);
		
		addContactListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				LinphoneActivity.instance().displayContactsForEdition(mAddress.getText().toString());
			}
		};
		cancelListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				LinphoneActivity.instance().resetClassicMenuLayoutAndGoBackToCallIfStillRunning();
			}
		};
		transferListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				LinphoneCore lc = LinphoneManager.getLc();
				
				LinphoneCall callToTransfer = lc.getCurrentCall();
				if (callToTransfer == null) {
					// TODO: This will only transfer the first paused call found.
					for (LinphoneCall c: lc.getCalls()) {
						if (State.Pausing.equals(c.getState()) || State.Paused.equals(c.getState())) {
							callToTransfer = c;
							break;
						}
					}
				}

				if (callToTransfer == null) {
					Log.e("No active or paused calls found to transfer!");
					return;
				}
				
				lc.transferCall(callToTransfer, mAddress.getText().toString());
				isCallTransferOngoing = false;
				LinphoneActivity.instance().resetClassicMenuLayoutAndGoBackToCallIfStillRunning();
			}
		};
		
		mAddContact.setEnabled(!(LinphoneActivity.isInstanciated() && LinphoneManager.getLc().getCallsNb() > 0));
		resetLayout(isCallTransferOngoing);
		
		if (getArguments() != null) {
			shouldEmptyAddressField = false;
			String number = getArguments().getString("SipUri");
			String displayName = getArguments().getString("DisplayName");
			String photo = getArguments().getString("PhotoUri");
			mAddress.setText(number);
			if (displayName != null) {
				mAddress.setDisplayedName(displayName);
			}
			if (photo != null) {
				mAddress.setPictureUri(Uri.parse(photo));
			}
		}

		return view;
    }

	/**
	 * @return null if not ready yet
	 */
	public static DialerFragment instance() { 
		return instance;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (LinphoneActivity.isInstanciated()) {
			LinphoneActivity.instance().updateDialerFragment(this);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (LinphoneActivity.isInstanciated()) {
			LinphoneActivity.instance().selectMenu(FragmentsAvailable.DIALER);
			LinphoneActivity.instance().updateDialerFragment(this);
		}
		
		if (shouldEmptyAddressField) {
			mAddress.setText("");
		} else {
			shouldEmptyAddressField = true;
		}
		resetLayout(isCallTransferOngoing);
	}
	
	public void resetLayout(boolean callTransfer) {
		isCallTransferOngoing = callTransfer;
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc == null) {
			return;
		}
	
		if (lc.getCallsNb() > 0) {
			this.adjustDtmfStatus(false);
			
			if (isCallTransferOngoing) {			
				mCall.setImageResource(R.drawable.transfer_call);
				mCall.setExternalClickListener(transferListener);
			} else {				
				mCall.setImageResource(R.drawable.add_call);
				mCall.resetClickListener();
			}
			mAddContact.setEnabled(true);
			mAddContact.setImageResource(R.drawable.cancel);
			mAddContact.setOnClickListener(cancelListener);
		} else {
			this.adjustDtmfStatus(true);
			
			mCall.setImageResource(R.drawable.call);
			mAddContact.setEnabled(true);
			mAddContact.setImageResource(R.drawable.add_contact);
			mAddContact.setOnClickListener(addContactListener);
			enableDisableAddContact();
		}
	}
	
	public void adjustDtmfStatus(boolean enabled) {
		if (numpad != null && numpad instanceof Numpad) {
			((Numpad) numpad).adjustPlayDtmf(enabled);
		}
	}
	
	public void enableDisableAddContact() {
		mAddContact.setEnabled(LinphoneManager.getLc().getCallsNb() > 0 || !mAddress.getText().toString().equals(""));	
	}
	
	public void newOutgoingCall(Intent intent) {
		if (intent != null) {
			boolean handled = false;
		
			Bundle extras = intent.getExtras();
			if (extras != null && extras.getString(Intent.EXTRA_PHONE_NUMBER) != null) {
				mAddress.setText(extras.getString(Intent.EXTRA_PHONE_NUMBER));
			
				handled = true;
			} else if (intent.getData() != null) {
				String scheme = intent.getData().getScheme();
				if (scheme.startsWith("imto")) {
					mAddress.setText("sip:" + intent.getData().getLastPathSegment());
				} else if (scheme.startsWith("call") || scheme.startsWith("sip")) {
					mAddress.setText(intent.getData().getSchemeSpecificPart());
				} else {
					Log.e("Unknown scheme: ",scheme);
					mAddress.setText(intent.getData().getSchemeSpecificPart());
				}
				
				handled = true;
			}
			
			if (handled) {
				mAddress.clearDisplayedName();
				intent.setData(null);
		
				LinphoneManager.getInstance().newOutgoingCall(mAddress);
			}
		}
	}
}
