package com.vimtura.vimphone.status;

import android.graphics.Color;

import com.vimtura.vimphone.LinphoneManager;
import com.vimtura.vimphone.LinphonePreferenceManager;
import com.vimtura.vimphone.R;

public enum StatusType {
	ORANGE(StatusTypeGroup.WARNING, R.drawable.status_orange, null, null, null, null),
	GREEN(StatusTypeGroup.SUCCESS, R.drawable.status_green, null, Color.CYAN, 300, 100),
	RED(StatusTypeGroup.ERROR, R.drawable.status_red, null, Color.RED, 300, 100),
	OFFLINE(StatusTypeGroup.ERROR, R.drawable.status_offline, null, null, null, null);
	
	private StatusTypeGroup group;
	private Integer smallIcon;
	private Integer largeIcon;
	private Integer lightRgb;
	private Integer lightOnMs;
	private Integer lightOffMs;
	
	StatusType(StatusTypeGroup group, Integer smallIcon, Integer largeIcon, Integer lightRgb, Integer lightOnMs, Integer lightOffMs) {
		this.group = group;
		this.smallIcon = smallIcon;
		this.largeIcon = largeIcon;
		this.lightRgb = lightRgb;
		this.lightOnMs = lightOnMs;
		this.lightOffMs = lightOffMs;
	}

	public StatusTypeGroup getGroup() {
		return group;
	}
	
	public Integer getSmallIcon() {
		return smallIcon;
	}

	public Integer getLargeIcon() {
		return largeIcon;
	}
	
	public boolean isLedEnabled() {
		if (lightRgb != null) {
			LinphonePreferenceManager prefManager = LinphoneManager.getPreferenceManager();
			if (prefManager != null) {
				switch (group) {
					case SUCCESS:
						if (prefManager.isStatusLedSuccessEnabled()) {
							return true;
						}
						break;
					case WARNING:
						if (prefManager.isStatusLedWarningEnabled()) {
							return true;
						}
						break;
					case ERROR:
					default:
						if (prefManager.isStatusLedErrorEnabled()) {
							return true;
						}
						break;
				}
			}
		}
		
		return false;
	}

	public Integer getLightRgb() {
		return lightRgb;
	}

	public Integer getLightOnMs() {
		return lightOnMs;
	}

	public Integer getLightOffMs() {
		return lightOffMs;
	}
	
}
