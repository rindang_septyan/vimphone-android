package com.vimtura.vimphone.header;

import org.linphone.core.LinphoneCall;
import org.linphone.mediastream.Log;

import android.annotation.SuppressLint;
import com.vimtura.vimphone.LinphoneManager;

import java.util.Map;
import java.util.HashMap;

@SuppressLint("UseSparseArrays")
public class VimturaHeaderFactory {
	
	protected final static Map<Integer, VimturaHeader> callHeaderMap = new HashMap<Integer, VimturaHeader>();
	
	public static VimturaHeader get(LinphoneCall call) {
		VimturaHeader header = null;
		
		// Clean up any old calls
		cleanup();		
		
		synchronized (callHeaderMap) {
			if (callHeaderMap.containsKey(call.hashCode())) {
				header = callHeaderMap.get(call.hashCode());
			}
			
			if (header == null) {	
				header = new VimturaHeader(call);
				
				callHeaderMap.put(call.hashCode(), header);
			}
		}
		
		return header;
	}
	
	public static void cleanup() {
		synchronized (callHeaderMap) {
			// Build list of current calls
			Map<Integer, Boolean> currentCallIds = new HashMap<Integer, Boolean>();
			
			for (LinphoneCall c: LinphoneManager.getLc().getCalls()) {
				currentCallIds.put(c.hashCode(), Boolean.TRUE);
			}
			
			// Remove any calls that aren't active
			for (int oldCallId: callHeaderMap.keySet()) {
				if (!currentCallIds.containsKey(oldCallId)) {
					callHeaderMap.remove(oldCallId);
				}
			}
		}
	}
	
}
